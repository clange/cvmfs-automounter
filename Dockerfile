#
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
#
MAINTAINER Daniel Juarez Gonzalez <djuarezg@cern.ch>

COPY  cvmfs.repo cvmfs-config.repo /etc/yum.repos.d/

RUN yum install -y cvmfs cvmfs-config && yum clean all

# we need to create the cvmfs automount _inside_ a shared mount
COPY auto.master /tmp/auto.master
RUN cat /tmp/auto.master >> /etc/auto.master

# config files from gitlabci01 /etc/cvmfs
COPY default.local /etc/cvmfs/default.local

# Add CMS specific config for CERN
COPY cms.cern.ch.local /etc/cvmfs/config.d/cms.cern.ch.local

#run automounter process in foreground
CMD /usr/sbin/automount -f --debug

